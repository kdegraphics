/* Defined if we have the 0.7 version of the Poppler library */
#cmakedefine HAVE_POPPLER_0_7 1

/* Defined if we have the 0.9 version of the Poppler library */
#cmakedefine HAVE_POPPLER_0_9 1
